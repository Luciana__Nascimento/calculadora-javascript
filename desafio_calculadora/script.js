//variaveis para armazenar valor, o operador e o estado da calculadora
let setValue = '0' // valor que será apresentado no display
let newValue = true // indica se o próximo dígito será de um novo número
let previousValue = 0 //valor acumulado para uma operação
let pendingOperation = null //operação acumulada

//atualização do visor
const displayUpdate = () => {

    let [intPart, floatPart] = setValue.split(',') //separar valor inteiro da parte decimal


    //para não ultarpassar tamanho da tela
    if (intPart.length > 13){
        document.getElementById('input-value').value = 'Erro'
        return
    }

    //para separar valores de milhares
    let valueThousand = ''
    counter = 0
    for (let i = intPart.length - 1; i >= 0; i--){
        if (++ counter > 3){
            valueThousand  = '.' + valueThousand
            counter = 1
        }
        valueThousand = intPart[i] + valueThousand
    }
    valueThousand = valueThousand + (floatPart ? ',' + floatPart.substring(0, 2) : '')
    document.getElementById('input-value').value = valueThousand
}


// tratamento do clique no botao digito
const digit = (number) =>{
    if(newValue){
        setValue = '' + number
        newValue = false
    }else 
    setValue += number
    displayUpdate()
}

// tratamento do clique no botao decimal
const point = () =>{
    if (newValue) {
        setValue = '0,'
        newValue = false
    }else if (setValue.indexOf(',') == -1) //para não incluir duas virgulas
        setValue += ','
        displayUpdate()
}

// tratamento do clique no botao ac
const allClear = () => {
    newValue = true
    previousValue = 0
    setValue = '0'
    pendingOperation = null
    displayUpdate()
}

// tratamento do clique no botao c
onload = () => {
    document.querySelector('#bt-clear').onclick = clear
}
const clear = () => {
    newValue=true
    setValue = setValue.substring(0, setValue.length - 1)
    displayUpdate()
}


// converte a string do valor para um número real
const currentValue = () => parseFloat(setValue.replace(',', '.'))


//tratamento do clique nos botões de operadores
const operator = (operator) => {
    calculate()
    previousValue = currentValue()
    pendingOperation = operator
    newValue = true
    //acumula nova operação
}

//tratamento do clique no botão =
const calculate = () =>{
    if(pendingOperation != null){
        let result
        switch(pendingOperation){
            case '+': result = previousValue + currentValue(); break
            case '-': result = previousValue - currentValue(); break
            case '*': result = previousValue * currentValue(); break
            case '/': result = previousValue / currentValue(); break
            case '%': result = previousValue * (currentValue()/100); break
        }
        setValue = result.toString().replace('.',',')
    }

    newValue = true
    pendingOperation = null
    previousValue = 0
    displayUpdate()
}
